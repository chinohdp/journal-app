import React from 'react';
import { Link } from 'react-router-dom';

export const RegisterScreen = () => {
  return (
    <>
      <h3 className="auth__title">Register Screen</h3>
      <form>
        <input
          className="auth__input"
          type="text"
          placeholder="Email"
          name="email"
          autoComplete="off"
        />
        <input
          className="auth__input"
          type="text"
          placeholder="Name"
          name="name"
          autoComplete="off"
        />

        <input
          className="auth__input"
          type="Password"
          placeholder="Password"
          name="password"
        />

        <input
          className="auth__input"
          type="Password"
          placeholder="Confirm password"
          name="passwordConfirm"
        />

        <button
          type="submit"
          className="btn btn-primary btn-block mb-5"
          disabled={false}
        >
          Register
        </button>

        <Link to="/auth/login" className="link ">
          Already registered?
        </Link>
      </form>
    </>
  );
};
